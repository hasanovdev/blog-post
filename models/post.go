package models

type Post struct {
	Id          string   `json:"id"`
	Description string   `json:"description"`
	Photos      []string `json:"photos"`
	Likes       int      `json:"likes"`
	CreatedAt   string   `json:"created_at"`
	CreatedBy   string   `json:"created_by"`
	UpdatedAt   string   `json:"updated_at"`
	UpdatedBy   string   `json:"updated_by"`
	DeletedAt   string   `json:"deleted_at"`
	DeletedBy   string   `json:"deleted_by"`
}

type PostCreateReq struct {
	Description string   `json:"description"`
	Photos      []string `json:"photos"`
	CreatedBy   string   `json:"-"`
}

type PostCreateResp struct {
	Msg string `json:"msg"`
}

type PostGetListReq struct {
	Page   int    `json:"page"`
	Limit  int    `json:"limit"`
	Search string `json:"search"`
}

type PostGetListResp struct {
	Posts []*Post `json:"posts"`
	Count int     `json:"count"`
}

type PostIdReq struct {
	Id string `json:"id"`
}

type PostUpdateReq struct {
	Id          string   `json:"id"`
	Description string   `json:"description"`
	Photos      []string `json:"photos"`
	UpdatedBy   string   `json:"-"`
}

type PostUpdateResp struct {
	Msg string `json:"msg"`
}

type PostDeleteReq struct {
	Id        string `json:"id"`
	DeletedBy string `json:"-"`
}

type PostDeleteResp struct {
	Msg string `json:"msg"`
}

type MyPostsGetListReq struct {
	Page   int    `json:"page"`
	Limit  int    `json:"limit"`
	Search string `json:"search"`
	UserId string `json:"user_id"`
}
