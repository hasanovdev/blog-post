package models

type User struct {
	Id        string `json:"id"`
	Username  string `json:"username"`
	Password  string `json:"password"`
	IsActive  bool   `json:"is_active"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
	DeletedAt string `json:"deleted_at"`
}

type UserCreateReq struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type UserCreateResp struct {
	Msg string `json:"msg"`
}

type SignInReq struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type ReqByUsername struct {
	Username string `json:"username"`
}
