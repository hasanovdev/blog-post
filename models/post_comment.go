package models

type Comment struct {
	Id          int
	PostId      string `json:"post_id"`
	UserId      string `json:"-"`
	Description string `json:"description"`
	Likes       int    `json:"likes"`
	CreatedAt   string `json:"created_at"`
	UpdatedAt   string `json:"-"`
	DeletedAt   string `json:"-"`
	DeletedBy   string `json:"-"`
}

type CommentCreateReq struct {
	PostId      string `json:"-"`
	UserId      string `json:"-"`
	Description string `json:"description"`
}

type CommentGetListReq struct {
	PostId string `json:"post_id"`
	Page   int    `json:"page"`
	Limit  int    `json:"limit"`
}

type CommentGetListResp struct {
	Comments []*Comment `json:"comments"`
	Count    int        `json:"count"`
}

type CommentUpdateReq struct {
	Id          int    `json:"id"`
	Description string `json:"description"`
}

type CommentIdReq struct {
	Id string `json:"id"`
}
