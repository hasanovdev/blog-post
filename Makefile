build:
	@go build -o ./bin/main cmd/main.go

run: build
	@./bin/main

swag:
	@swag init -g api/api.go -o api/docs

migrate-up:
	@migrate -database postgres://postgres:password@localhost:5432/blog_post\?sslmode\=disable -path schema up

migrate-down:
	@migrate -database postgres://postgres:password@localhost:5432/blog_post\?sslmode\=disable -path schema down