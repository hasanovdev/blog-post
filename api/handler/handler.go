package handler

import (
	"blog-post/config"
	"blog-post/pkg/logger"
	"blog-post/storage"

	"github.com/gin-gonic/gin"
)

type Handler struct {
	strg storage.StorageI
	cfg  config.Config
	log  logger.LoggerI
}

func NewHandler(strg storage.StorageI, cfg config.Config, log logger.LoggerI) *Handler {
	return &Handler{strg: strg, cfg: cfg, log: log}
}

type Response struct {
	Status      int         `json:"status"`
	Description string      `json:"description"`
	Data        interface{} `json:"data"`
}

func (h *Handler) handlerResponse(c *gin.Context, path string, code int, message interface{}) {
	response := Response{
		Status: code,
		Data:   message,
	}

	switch {
	case code < 300:
		h.log.Info(path, logger.Any("info", response))
	case code >= 400:
		h.log.Error(path, logger.Any("error", response))
	}

	c.JSON(code, response)
}
