package handler

import (
	"blog-post/config"
	"blog-post/models"
	"blog-post/pkg/helper"
	"blog-post/pkg/logger"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

// CreatePost godoc
// @Summary      Create a new post
// @Security ApiKeyAuth
// @Router       /posts [post]
// @Description  Create a new post with the provided details
// @Tags         posts
// @Accept       json
// @Produce      json
// @Param        post     body  models.PostCreateReq  true  "data of the post"
// @Success      201  {string}  Response{data=string}
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) CreatePost(ctx *gin.Context) {
	var post = models.Post{}

	tokenString := ctx.Request.Header.Get("Authorization")

	token, err := helper.ExtractToken(tokenString)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	claims, err := helper.ParseClaims(token, config.JWTSecretKey)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	err = ctx.ShouldBindJSON(&post)
	if err != nil {
		h.handlerResponse(ctx, "CreatePost", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.strg.Post().Create(ctx, &models.PostCreateReq{
		Description: post.Description,
		Photos:      post.Photos,
		CreatedBy:   claims.UserID,
	})

	if err != nil {
		h.handlerResponse(ctx, "CreatePost", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "CreatePost", http.StatusOK, resp)
}

// ListPosts godoc
// @Summary      List posts
// @Router       /posts [get]
// @Description  get posts
// @Tags         posts
// @Accept       json
// @Produce      json
// @Param        limit    query     int  false  "limit for response"  Default(10)
// @Param		 page     query     int  false  "page for response"   Default(1)
// @Param        search     query     string false "search by description"
// @Success      200  {array}   models.Post
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) GetListPost(ctx *gin.Context) {
	page, err := strconv.Atoi(ctx.DefaultQuery("page", "1"))
	if err != nil {
		h.log.Error("error get page:", logger.Error(err))
		ctx.JSON(http.StatusBadRequest, "invalid page param")
		return
	}

	limit, err := strconv.Atoi(ctx.DefaultQuery("limit", "10"))
	if err != nil {
		h.log.Error("error get limit:", logger.Error(err))
		ctx.JSON(http.StatusBadRequest, "invalid page param")
		return
	}

	resp, err := h.strg.Post().GetList(ctx.Request.Context(), &models.PostGetListReq{
		Page:   page,
		Limit:  limit,
		Search: ctx.Query("search"),
	})

	for _, post := range resp.Posts {
		count, _ := h.strg.PostLike().Count(ctx.Request.Context(), post.Id)
		post.Likes = count
	}

	if err != nil {
		h.log.Error("error Post GetList:", logger.Error(err))
		ctx.JSON(http.StatusInternalServerError, "internal server error")
		return
	}
	h.log.Info("response to GetListPost")
	ctx.JSON(http.StatusOK, resp)
}

// GetPost godoc
// @Summary      Get a post by ID
// @Security ApiKeyAuth
// @Router       /posts/{id} [get]
// @Description  Retrieve a post by its unique identifier
// @Tags         posts
// @Accept       json
// @Produce      json
// @Param        id   path    string     true    "Post ID to retrieve"
// @Success      200  {object}  models.Post
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) GetPost(ctx *gin.Context) {
	id := ctx.Param("id")

	postLikes, err := h.strg.PostLike().Count(ctx.Request.Context(), id)
	if err != nil {
		h.log.Error("error PostLike().Count :", logger.Error(err))
		ctx.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	post, err := h.strg.Post().GetById(ctx.Request.Context(), &models.PostIdReq{
		Id: id,
	})
	if err != nil {
		h.log.Error("error Post GetById:", logger.Error(err))
		ctx.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	post.Likes = postLikes

	h.log.Info("response to GetPost")
	ctx.JSON(http.StatusOK, post)
}

// UpdatePost godoc
// @Summary      Update an existing post
// @Security ApiKeyAuth
// @Router       /posts/{id} [put]
// @Description  Update an existing post with the provided details
// @Tags         posts
// @Accept       json
// @Produce      json
// @Param        id       path    string     true    "Post ID to update"
// @Param        post   body    models.PostUpdateReq  true    "Updated data for the post"
// @Success      200  {object}  models.PostUpdateResp
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) UpdatePost(ctx *gin.Context) {
	var post = &models.Post{}
	tokenString := ctx.Request.Header.Get("Authorization")

	token, err := helper.ExtractToken(tokenString)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	claims, err := helper.ParseClaims(token, config.JWTSecretKey)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	post.Id = ctx.Param("id")
	if err := ctx.ShouldBindJSON(&post); err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		ctx.JSON(http.StatusBadRequest, "invalid body")
		return
	}

	res, err := h.strg.Post().GetById(ctx.Request.Context(), &models.PostIdReq{Id: post.Id})
	if err != nil {
		h.log.Error("error while getting post from id:", logger.Error(err))
		ctx.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	if res.CreatedBy != claims.UserID {
		ctx.JSON(http.StatusForbidden, "you cannot edit this post")
		return
	}

	resp, err := h.strg.Post().Update(ctx.Request.Context(), &models.PostUpdateReq{
		Id:          post.Id,
		Description: post.Description,
		Photos:      post.Photos,
		UpdatedBy:   claims.UserID,
	})
	if err != nil {
		h.log.Error("error update post:", logger.Error(err))
		ctx.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	h.log.Info("response to UpdatePost")
	ctx.JSON(http.StatusOK, resp)
}

// DeletePost godoc
// @Summary      Delete a post
// @Security ApiKeyAuth
// @Router       /posts/{id} [delete]
// @Description  delete a post by its unique identifier
// @Tags         posts
// @Accept       json
// @Produce      json
// @Param        id   path    string     true    "Post ID to retrieve"
// @Success      200  {object}  models.PostDeleteResp
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) DeletePost(ctx *gin.Context) {
	id := ctx.Param("id")

	tokenString := ctx.Request.Header.Get("Authorization")

	token, err := helper.ExtractToken(tokenString)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	claims, err := helper.ParseClaims(token, config.JWTSecretKey)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	resp, err := h.strg.Post().Delete(ctx.Request.Context(), &models.PostDeleteReq{
		Id:        id,
		DeletedBy: claims.UserID,
	})

	if err != nil {
		h.log.Error("error Post Delete:", logger.Error(err))
		ctx.JSON(http.StatusInternalServerError, "internal server error")
		return
	}
	h.log.Info("response to DeletePost")
	ctx.JSON(http.StatusOK, resp)
}

// ListMyPosts godoc
// @Summary      List my posts
// @Security ApiKeyAuth
// @Router       /my/posts [get]
// @Description  get my posts
// @Tags         posts
// @Accept       json
// @Produce      json
// @Param        limit    query     int  false  "limit for response"  Default(10)
// @Param		 page     query     int  false  "page for response"   Default(1)
// @Param        search     query     string false "search by description"
// @Success      200  {array}   models.Post
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) GetListMyPosts(ctx *gin.Context) {
	page, err := strconv.Atoi(ctx.DefaultQuery("page", "1"))
	if err != nil {
		h.log.Error("error get page:", logger.Error(err))
		ctx.JSON(http.StatusBadRequest, "invalid page param")
		return
	}

	limit, err := strconv.Atoi(ctx.DefaultQuery("limit", "10"))
	if err != nil {
		h.log.Error("error get limit:", logger.Error(err))
		ctx.JSON(http.StatusBadRequest, "invalid page param")
		return
	}

	tokenString := ctx.Request.Header.Get("Authorization")

	token, err := helper.ExtractToken(tokenString)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	claims, err := helper.ParseClaims(token, config.JWTSecretKey)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	resp, err := h.strg.Post().GetListPostsByUserId(ctx.Request.Context(), &models.MyPostsGetListReq{
		Page:   page,
		Limit:  limit,
		Search: ctx.Query("search"),
		UserId: claims.UserID,
	})

	for _, post := range resp.Posts {
		count, _ := h.strg.PostLike().Count(ctx.Request.Context(), post.Id)
		post.Likes = count
	}

	if err != nil {
		h.log.Error("error Post GetList:", logger.Error(err))
		ctx.JSON(http.StatusInternalServerError, "internal server error")
		return
	}
	h.log.Info("response to GetListPost")
	ctx.JSON(http.StatusOK, resp)
}

// ListUsersPosts godoc
// @Summary      List user's posts
// @Router       /{username}/posts [get]
// @Description  get user's posts
// @Tags         posts
// @Accept       json
// @Produce      json
// @Param        username     path     string true "get posts by username"
// @Param        limit    query     int  false  "limit for response"  Default(10)
// @Param		 page     query     int  false  "page for response"   Default(1)
// @Param        search     query     string false "search by description"
// @Success      200  {array}   models.Post
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) GetUserPosts(ctx *gin.Context) {
	username := ctx.Param("username")

	page, err := strconv.Atoi(ctx.DefaultQuery("page", "1"))
	if err != nil {
		h.log.Error("error get page:", logger.Error(err))
		ctx.JSON(http.StatusBadRequest, "invalid page param")
		return
	}

	limit, err := strconv.Atoi(ctx.DefaultQuery("limit", "10"))
	if err != nil {
		h.log.Error("error get limit:", logger.Error(err))
		ctx.JSON(http.StatusBadRequest, "invalid page param")
		return
	}

	user, err := h.strg.User().GetByUsername(ctx.Request.Context(), &models.ReqByUsername{
		Username: username,
	})
	if err != nil {
		h.log.Error("error get user:", logger.Error(err))
		ctx.JSON(http.StatusBadRequest, "invalid username param")
		return
	}

	resp, err := h.strg.Post().GetListPostsByUserId(ctx.Request.Context(), &models.MyPostsGetListReq{
		Page:   page,
		Limit:  limit,
		Search: ctx.Query("search"),
		UserId: user.Id,
	})

	for _, post := range resp.Posts {
		count, _ := h.strg.PostLike().Count(ctx.Request.Context(), post.Id)
		post.Likes = count
	}

	if err != nil {
		h.log.Error("error user's Post GetList:", logger.Error(err))
		ctx.JSON(http.StatusInternalServerError, "internal server error")
		return
	}
	h.log.Info("response to user's GetListPost")
	ctx.JSON(http.StatusOK, resp)
}
