package handler

import (
	"blog-post/config"
	"blog-post/models"
	"blog-post/pkg/helper"
	"blog-post/pkg/logger"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

// CreatePostComment godoc
// @Summary      Create a new comment for post
// @Security ApiKeyAuth
// @Router       /comments/{post_id} [post]
// @Description  Create a new post comment with the provided details
// @Tags         post_comments
// @Accept       json
// @Produce      json
// @Param        post_id     path  string true "post_id"
// @Param        comment     body  models.CommentCreateReq  true  "data of the post"
// @Success      201  {string}  Response{data=string}
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) CreateComment(ctx *gin.Context) {
	postId := ctx.Param("post_id")

	var postComment = models.CommentCreateReq{}

	tokenString := ctx.Request.Header.Get("Authorization")

	token, err := helper.ExtractToken(tokenString)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	claims, err := helper.ParseClaims(token, config.JWTSecretKey)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	err = ctx.ShouldBindJSON(&postComment)
	if err != nil {
		h.handlerResponse(ctx, "CreateComment", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.strg.PostComment().Create(ctx.Request.Context(), &models.CommentCreateReq{
		PostId:      postId,
		UserId:      claims.UserID,
		Description: postComment.Description,
	})

	if err != nil {
		h.handlerResponse(ctx, "CreatePostComment", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "CreatePostComment", http.StatusOK, resp)
}

// GetPostComment godoc
// @Summary      Get a comment by ID
// @Security ApiKeyAuth
// @Router       /comments/{id} [get]
// @Description  Retrieve a comment by its unique identifier
// @Tags         comments
// @Accept       json
// @Produce      json
// @Param        id   path    string     true    "Comment ID to retrieve"
// @Success      200  {object}  models.Comment
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) GetComment(ctx *gin.Context) {
	id := ctx.Param("id")

	commentLikes, err := h.strg.CommentLike().Count(ctx.Request.Context(), id)
	if err != nil {
		h.log.Error("error CommentLike().Count :", logger.Error(err))
		ctx.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	comment, err := h.strg.PostComment().GetById(ctx.Request.Context(), &models.CommentIdReq{
		Id: id,
	})

	if err != nil {
		h.log.Error("error Post GetById:", logger.Error(err))
		ctx.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	comment.Likes = commentLikes

	h.log.Info("response to GetComment")
	ctx.JSON(http.StatusOK, comment)
}

// ListPostComments godoc
// @Summary      List post comments
// @Router       /comments/{post_id} [get]
// @Description  get postComments
// @Tags         post_comments
// @Accept       json
// @Produce      json
// @Param        post_id     path  string true "post_id"
// @Param        limit    query     int  false  "limit for response"  Default(10)
// @Param		 page     query     int  false  "page for response"   Default(1)
// @Success      200  {array}   models.Comment
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) GetListComment(ctx *gin.Context) {
	postId := ctx.Param("post_id")

	page, err := strconv.Atoi(ctx.DefaultQuery("page", "1"))
	if err != nil {
		h.log.Error("error get page:", logger.Error(err))
		ctx.JSON(http.StatusBadRequest, "invalid page param")
		return
	}

	limit, err := strconv.Atoi(ctx.DefaultQuery("limit", "10"))
	if err != nil {
		h.log.Error("error get limit:", logger.Error(err))
		ctx.JSON(http.StatusBadRequest, "invalid page param")
		return
	}

	resp, err := h.strg.PostComment().GetList(ctx.Request.Context(), &models.CommentGetListReq{
		Page:   page,
		Limit:  limit,
		PostId: postId,
	})

	for _, comment := range resp.Comments {
		count, _ := h.strg.CommentLike().Count(ctx.Request.Context(), fmt.Sprintf("%d", comment.Id))
		comment.Likes = count
	}

	if err != nil {
		h.log.Error("error PostComment GetList:", logger.Error(err))
		ctx.JSON(http.StatusInternalServerError, "internal server error")
		return
	}

	h.log.Info("response to GetListPostComment")
	ctx.JSON(http.StatusOK, resp)
}
