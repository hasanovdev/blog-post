package handler

import (
	"blog-post/config"
	"blog-post/models"
	"blog-post/pkg/helper"
	"net/http"

	"github.com/gin-gonic/gin"
)

// sign in user handler
// @Router       /auth/sign-in [post]
// @Summary      sign in user
// @Description  api for sign in user
// @Tags         auth
// @Accept       json
// @Produce      json
// @Param        user    body     models.SignInReq  true  "data of user"
// @Success      200  {object}  Response{data=string}
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) SignIn(ctx *gin.Context) {
	var req models.SignInReq
	err := ctx.ShouldBindJSON(&req)
	if err != nil {
		h.handlerResponse(ctx, "SignIn binding request", http.StatusBadRequest, err.Error())
		return
	}

	user, err := h.strg.User().GetByUsername(ctx.Request.Context(), &models.ReqByUsername{
		Username: req.Username,
	})

	if err != nil {
		h.handlerResponse(ctx, "SignIn get user by username", http.StatusBadRequest, err.Error())
		return
	}

	err = helper.ComparePasswords([]byte(user.Password), []byte(req.Password))
	if err != nil {
		h.handlerResponse(ctx, "SignIn compare passwords", http.StatusBadRequest, err.Error())
		return
	}

	m := make(map[string]interface{})
	m["user_id"] = user.Id
	m["is_active"] = user.IsActive

	token, err := helper.GenerateJWT(m, config.TokenExpireTime, config.JWTSecretKey)
	if err != nil {
		h.handlerResponse(ctx, "SignIn generate jwt", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "SignIn", http.StatusOK, token)
}

// sign up user handler
// @Router       /auth/sign-up [post]
// @Summary      sign up user
// @Description  api for sign up user
// @Tags         auth
// @Accept       json
// @Produce      json
// @Param        user    body     models.UserCreateReq  true  "data of user"
// @Success      200  {object}  Response{data=string}
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) SignUp(ctx *gin.Context) {
	var input = models.UserCreateReq{}

	if err := ctx.BindJSON(&input); err != nil {
		h.handlerResponse(ctx, "SignUp binding", http.StatusBadRequest, err.Error())
		return
	}

	hash, err := helper.GeneratePasswordHash(input.Password)
	if err != nil {
		h.handlerResponse(ctx, "SignUp generate hash password", http.StatusBadRequest, err.Error())
		return
	}
	input.Password = string(hash)

	id, err := h.strg.User().Create(ctx.Request.Context(), &input)
	if err != nil {
		h.handlerResponse(ctx, "SignUp User Create", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "SignUp", http.StatusOK, id)
}
