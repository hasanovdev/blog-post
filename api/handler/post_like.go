package handler

import (
	"blog-post/config"
	"blog-post/pkg/helper"
	"net/http"

	"github.com/gin-gonic/gin"
)

// LikePost godoc
// @Summary      Like a post
// @Security ApiKeyAuth
// @Router       /like-post/{post_id} [post]
// @Description  Like a post
// @Tags         post_likes
// @Accept       json
// @Produce      json
// @Param        post_id     path  string true "post_id"
// @Success      201  {string}  Response{data=string}
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) LikePost(ctx *gin.Context) {
	postId := ctx.Param("post_id")

	tokenString := ctx.Request.Header.Get("Authorization")

	token, err := helper.ExtractToken(tokenString)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	claims, err := helper.ParseClaims(token, config.JWTSecretKey)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if err := h.strg.PostLike().Check(ctx.Request.Context(), claims.UserID, postId); err != nil {
		if err = h.strg.PostLike().Push(ctx.Request.Context(), claims.UserID, postId); err != nil {
			h.handlerResponse(ctx, "PostLike().Push", http.StatusBadRequest, err.Error())
			return
		}

		h.log.Info("like a post")
		ctx.JSON(http.StatusOK, "like a post")
	} else {
		if err := h.strg.PostLike().Pop(ctx.Request.Context(), claims.UserID, postId); err != nil {
			h.handlerResponse(ctx, "PostLike().Pop", http.StatusBadRequest, err.Error())
			return
		}

		h.log.Info("unlike a post")
		ctx.JSON(http.StatusOK, "unlike a post")
	}
}
