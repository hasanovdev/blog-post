package handler

import (
	"blog-post/config"
	"blog-post/pkg/helper"
	"net/http"

	"github.com/gin-gonic/gin"
)

// LikeComment godoc
// @Summary      Like a comment
// @Security ApiKeyAuth
// @Router       /like-comment/{comment_id} [post]
// @Description  Like a comment
// @Tags         comment_likes
// @Accept       json
// @Produce      json
// @Param        comment_id     path  string true "comment_id"
// @Success      201  {string}  Response{data=string}
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) LikeComment(ctx *gin.Context) {
	commentId := ctx.Param("comment_id")

	tokenString := ctx.Request.Header.Get("Authorization")

	token, err := helper.ExtractToken(tokenString)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	claims, err := helper.ParseClaims(token, config.JWTSecretKey)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if err := h.strg.CommentLike().Check(ctx.Request.Context(), claims.UserID, commentId); err != nil {
		if err = h.strg.CommentLike().Push(ctx.Request.Context(), claims.UserID, commentId); err != nil {
			h.handlerResponse(ctx, "CommentLike().Push", http.StatusBadRequest, err.Error())
			return
		}

		h.log.Info("like a comment")
		ctx.JSON(http.StatusOK, "like a comment")
	} else {
		if err := h.strg.CommentLike().Pop(ctx.Request.Context(), claims.UserID, commentId); err != nil {
			h.handlerResponse(ctx, "CommentLike().Pop", http.StatusBadRequest, err.Error())
			return
		}

		h.log.Info("unlike a comment")
		ctx.JSON(http.StatusOK, "unlike a comment")
	}
}
