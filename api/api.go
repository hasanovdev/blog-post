package api

import (
	"blog-post/api/handler"
	"blog-post/pkg/middleware"

	_ "blog-post/api/docs"

	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func NewServer(h *handler.Handler) *gin.Engine {
	r := gin.New()

	auth := r.Group("/auth")
	{
		auth.POST("/sign-in", h.SignIn)
		auth.POST("/sign-up", h.SignUp)
	}

	my := r.Group("/my")
	my.Use(middleware.AuthMiddleware)
	{
		my.GET("/posts", h.GetListMyPosts)
	}

	username := r.Group("/:username")
	{
		username.GET("/posts", h.GetUserPosts)
	}

	post := r.Group("/posts")
	post.GET("/", h.GetListPost)
	post.GET("/:id", h.GetPost)
	post.Use(middleware.AuthMiddleware)
	{
		post.POST("/", h.CreatePost)
		post.PUT("/:id", h.UpdatePost)
		post.DELETE("/:id", h.DeletePost)
	}

	likePost := r.Group("/like-post")
	likePost.Use(middleware.AuthMiddleware)
	{
		likePost.POST("/:post_id", h.LikePost)
	}

	likeComment := r.Group("/like-comment")
	likeComment.Use(middleware.AuthMiddleware)
	{
		likeComment.POST("/:comment_id", h.LikeComment)
	}

	comment := r.Group("/comments")
	{
		comment.POST("/:post_id", h.CreateComment)
		comment.GET("/:post_id", h.GetListComment)
	}

	url := ginSwagger.URL("swagger/doc.json") // The url pointing to API definition
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))
	return r
}
