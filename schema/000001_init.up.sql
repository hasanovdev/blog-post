CREATE TABLE "users" (
  "id" varchar(36) PRIMARY KEY,
  "username" varchar(30) NOT NULL,
  "password" varchar(60) NOT NULL,
  "is_active" bool NOT NULL DEFAULT (true),
  "created_at" timestamp NOT NULL DEFAULT (NOW()),
  "updated_at" timestamp,
  "deleted_at" timestamp
);

CREATE TABLE "posts" (
  "id" varchar(36) PRIMARY KEY,
  "description" varchar(300),
  "photos" text[],
  "created_at" timestamp NOT NULL DEFAULT (NOW()),
  "created_by" varchar(36),
  "updated_at" timestamp,
  "updated_by" varchar(36),
  "deleted_at" timestamp,
  "deleted_by" varchar(36)
);

CREATE TABLE "posts_likes" (
  "id" serial PRIMARY KEY,
  "post_id" varchar(36) NOT NULL,
  "user_id" varchar(36) NOT NULL,
  "created_at" timestamp NOT NULL DEFAULT (NOW())
);

CREATE TABLE "posts_comments" (
  "id" serial PRIMARY KEY,
  "post_id" varchar(36) NOT NULL,
  "user_id" varchar(36) NOT NULL,
  "description" varchar(300),
  "created_at" timestamp NOT NULL DEFAULT (NOW()),
  "created_by" varchar(36),
  "updated_at" timestamp,
  "deleted_at" timestamp,
  "deleted_by" varchar(36)
);

CREATE TABLE "comments_likes" (
  "id" serial PRIMARY KEY,
  "comment_id" int,
  "user_id" varchar(36) NOT NULL,
  "created_at" timestamp NOT NULL DEFAULT (NOW())
);

ALTER TABLE "posts" ADD FOREIGN KEY ("created_by") REFERENCES "users" ("id");

ALTER TABLE "posts" ADD FOREIGN KEY ("updated_by") REFERENCES "users" ("id");

ALTER TABLE "posts" ADD FOREIGN KEY ("deleted_by") REFERENCES "users" ("id");

ALTER TABLE "posts_likes" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");

ALTER TABLE "posts_likes" ADD FOREIGN KEY ("post_id") REFERENCES "posts" ("id");

ALTER TABLE "posts_comments" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");

ALTER TABLE "posts_comments" ADD FOREIGN KEY ("post_id") REFERENCES "posts" ("id");

ALTER TABLE "comments_likes" ADD FOREIGN KEY ("comment_id") REFERENCES "posts_comments" ("id");

ALTER TABLE "comments_likes" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");
