package storage

import (
	"blog-post/models"
	"context"
)

type StorageI interface {
	Post() PostsI
	User() UsersI
	PostLike() PostsLikesI
	PostComment() PostsCommentsI
	CommentLike() CommentsLikesI
}

type PostsI interface {
	Create(ctx context.Context, req *models.PostCreateReq) (*models.PostCreateResp, error)
	GetList(ctx context.Context, req *models.PostGetListReq) (resp *models.PostGetListResp, err error)
	GetById(ctx context.Context, req *models.PostIdReq) (*models.Post, error)
	Update(ctx context.Context, req *models.PostUpdateReq) (*models.PostUpdateResp, error)
	Delete(ctx context.Context, req *models.PostDeleteReq) (*models.PostDeleteResp, error)
	GetListPostsByUserId(ctx context.Context, req *models.MyPostsGetListReq) (*models.PostGetListResp, error)
}

type UsersI interface {
	Create(ctx context.Context, req *models.UserCreateReq) (*models.UserCreateResp, error)
	GetByUsername(ctx context.Context, req *models.ReqByUsername) (*models.User, error)
}

type PostsLikesI interface {
	Push(ctx context.Context, userId, postId string) error
	Pop(ctx context.Context, userId, postId string) error
	Check(ctx context.Context, userId, postId string) error
	Count(ctx context.Context, postId string) (count int, err error)
}

type PostsCommentsI interface {
	Create(ctx context.Context, req *models.CommentCreateReq) (resp *int, err error)
	GetList(ctx context.Context, req *models.CommentGetListReq) (*models.CommentGetListResp, error)
	GetById(ctx context.Context, req *models.CommentIdReq) (*models.Comment, error)
	Update(ctx context.Context, req *models.CommentUpdateReq) (resp *string, err error)
}

type CommentsLikesI interface {
	Push(ctx context.Context, userId, commentId string) error
	Pop(ctx context.Context, userId, commentId string) error
	Check(ctx context.Context, userId, commentId string) error
	Count(ctx context.Context, commentId string) (count int, err error)
}
