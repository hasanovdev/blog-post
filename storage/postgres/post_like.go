package postgres

import (
	"context"
	"errors"

	"github.com/jackc/pgx/v4/pgxpool"
)

type postLikeRepo struct {
	db *pgxpool.Pool
}

func NewPostLikeRepo(db *pgxpool.Pool) *postLikeRepo {
	return &postLikeRepo{
		db: db,
	}
}

func (r *postLikeRepo) Push(ctx context.Context, userId, postId string) error {
	query := `
	INSERT INTO posts_likes(
		user_id,
		post_id
	)
	VALUES($1,$2);`

	_, err := r.db.Exec(ctx, query, userId, postId)
	if err != nil {
		return err
	}

	return nil
}

func (r *postLikeRepo) Pop(ctx context.Context, userId, postId string) error {
	query := `
	DELETE FROM posts_likes
	WHERE user_id = $1 AND post_id = $2;`

	_, err := r.db.Exec(ctx, query, userId, postId)
	if err != nil {
		return err
	}

	return nil
}

func (r *postLikeRepo) Check(ctx context.Context, userId, postId string) error {
	var count int
	query := `
	SELECT COUNT(*) FROM posts_likes
	WHERE user_id = $1 AND post_id = $2;`

	if err := r.db.QueryRow(ctx, query, userId, postId).Scan(&count); err != nil {
		return err
	}

	if count == 0 {
		return errors.New("count=0")
	}

	return nil
}

func (r *postLikeRepo) Count(ctx context.Context, postId string) (count int, err error) {
	query := `
	SELECT COUNT(*) FROM posts_likes
	WHERE post_id = $1;`

	if err := r.db.QueryRow(ctx, query, postId).Scan(&count); err != nil {
		return 0, err
	}

	return count, nil
}
