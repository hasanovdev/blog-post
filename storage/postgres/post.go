package postgres

import (
	"blog-post/models"
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type postRepo struct {
	db *pgxpool.Pool
}

func NewPostRepo(db *pgxpool.Pool) *postRepo {
	return &postRepo{
		db: db,
	}
}

func (r *postRepo) Create(ctx context.Context, req *models.PostCreateReq) (*models.PostCreateResp, error) {
	id := uuid.NewString()

	query := `
	INSERT INTO posts(
		id,
        description,
        photos,
        created_by
	)
	VALUES ($1, $2, $3, $4);
	`

	_, err := r.db.Exec(ctx, query,
		id,
		req.Description,
		req.Photos,
		req.CreatedBy,
	)

	if err != nil {
		return nil, err
	}

	return &models.PostCreateResp{
		Msg: "post created with id: " + id,
	}, nil
}

func (r *postRepo) GetList(ctx context.Context, req *models.PostGetListReq) (*models.PostGetListResp, error) {
	var (
		filter  = " WHERE deleted_at IS NULL "
		offsetQ = " OFFSET 0;"
		limit   = " LIMIT 10 "
		offset  = (req.Page - 1) * req.Limit
		count   int
		resp    = &models.PostGetListResp{}
	)

	s := `
	SELECT 
		id,
		description,
		photos,
		created_at::TEXT,
		created_by,
		updated_at::TEXT,
		updated_by 
	FROM posts `

	if req.Search != "" {
		filter += ` AND description ILIKE ` + "'%" + req.Search + "%' "
	}
	if req.Limit > 0 {
		limit = fmt.Sprintf("LIMIT %d", req.Limit)
	}
	if offset > 0 {
		offsetQ = fmt.Sprintf("OFFSET %d", offset)
	}

	query := s + filter + limit + offsetQ

	countS := `SELECT COUNT(*) FROM posts ` + filter

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	err = r.db.QueryRow(ctx, countS).Scan(&count)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			id          string
			description string
			photos      []string
			createdAt   string
			createdBy   string
			updatedAt   sql.NullString
			updatedBy   sql.NullString
		)
		err := rows.Scan(
			&id,
			&description,
			&photos,
			&createdAt,
			&createdBy,
			&updatedAt,
			&updatedBy,
		)

		if err != nil {
			return nil, err
		}

		resp.Posts = append(resp.Posts, &models.Post{
			Id:          id,
			Description: description,
			Photos:      photos,
			CreatedAt:   createdAt,
			CreatedBy:   createdBy,
			UpdatedAt:   updatedAt.String,
			UpdatedBy:   updatedBy.String,
		})
		resp.Count = count
	}

	return resp, nil
}

func (r *postRepo) GetById(ctx context.Context, req *models.PostIdReq) (*models.Post, error) {
	var (
		id          string
		description string
		photos      []string
		createdAt   string
		createdBy   string
		updatedAt   sql.NullString
		updatedBy   sql.NullString
	)

	query := `
	SELECT 
		id,
		description,
		photos,
		created_at::TEXT,
		created_by,
		updated_at::TEXT,
		updated_by 
	FROM posts
	WHERE id = $1 AND deleted_at IS NULL;`

	if err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&description,
		&photos,
		&createdAt,
		&createdBy,
		&updatedAt,
		&updatedBy,
	); err != nil {
		return nil, err
	}

	return &models.Post{
		Id:          id,
		Description: description,
		Photos:      photos,
		CreatedAt:   createdAt,
		CreatedBy:   createdBy,
		UpdatedAt:   updatedAt.String,
		UpdatedBy:   updatedBy.String,
	}, nil
}

func (r *postRepo) Update(ctx context.Context, req *models.PostUpdateReq) (*models.PostUpdateResp, error) {
	query := `
    UPDATE posts
    SET description = $2,
        photos = $3,
        updated_by = $4,
		updated_at = NOW()
    WHERE id = $1;`

	_, err := r.db.Exec(ctx, query,
		req.Id,
		req.Description,
		req.Photos,
		req.UpdatedBy,
	)

	if err != nil {
		return nil, err
	}

	return &models.PostUpdateResp{
		Msg: "post updated",
	}, nil
}

func (r *postRepo) Delete(ctx context.Context, req *models.PostDeleteReq) (*models.PostDeleteResp, error) {
	query := `
	UPDATE posts
	SET 
		deleted_at = NOW(),
		deleted_by = $2
	WHERE id = $1;
	`
	_, err := r.db.Exec(ctx, query,
		req.Id,
		req.DeletedBy,
	)

	if err != nil {
		return nil, err
	}

	return &models.PostDeleteResp{
		Msg: "post deleted",
	}, nil
}

func (r *postRepo) GetListPostsByUserId(ctx context.Context, req *models.MyPostsGetListReq) (*models.PostGetListResp, error) {
	var (
		filter  = " WHERE deleted_at IS NULL "
		offsetQ = " OFFSET 0;"
		limit   = " LIMIT 10 "
		offset  = (req.Page - 1) * req.Limit
		count   int
		resp    = &models.PostGetListResp{}
	)

	s := `
	SELECT 
		id,
		description,
		photos,
		created_at::TEXT,
		created_by,
		updated_at::TEXT,
		updated_by 
	FROM posts `

	filter += ` AND created_by = '` + req.UserId + `' `

	if req.Search != "" {
		filter += ` AND description ILIKE ` + "'%" + req.Search + "%' "
	}
	if req.Limit > 0 {
		limit = fmt.Sprintf("LIMIT %d", req.Limit)
	}
	if offset > 0 {
		offsetQ = fmt.Sprintf("OFFSET %d", offset)
	}

	query := s + filter + limit + offsetQ

	countS := `SELECT COUNT(*) FROM posts ` + filter

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	err = r.db.QueryRow(ctx, countS).Scan(&count)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			id          string
			description string
			photos      []string
			createdAt   string
			createdBy   string
			updatedAt   sql.NullString
			updatedBy   sql.NullString
		)
		err := rows.Scan(
			&id,
			&description,
			&photos,
			&createdAt,
			&createdBy,
			&updatedAt,
			&updatedBy,
		)

		if err != nil {
			return nil, err
		}

		resp.Posts = append(resp.Posts, &models.Post{
			Id:          id,
			Description: description,
			Photos:      photos,
			CreatedAt:   createdAt,
			CreatedBy:   createdBy,
			UpdatedAt:   updatedAt.String,
			UpdatedBy:   updatedBy.String,
		})
		resp.Count = count
	}

	return resp, nil
}

func (r *postRepo) Like(ctx context.Context, userId, postId string) error {
	var count int
	countLikeQuery := `
	select count(*) from posts_likes
	where
	user_id='22eb9dfd-96fd-41c4-867c-23cc74004523' and post_id='bab39ad0-6294-4f65-b566-ed5de334b337';
	`

	_ = r.db.QueryRow(ctx, countLikeQuery).Scan(&count)

	if count > 0 {
		query := `
		update posts_likes
		set status='f'
		where user_id = $1 and post_id = $2;`

		_, err := r.db.Exec(ctx, query, userId, postId)
		if err != nil {
			return err
		}
	}

	query := `
	insert into posts_likes(user_id,post_id)
	values($1,$2);`

	_, err := r.db.Exec(ctx, query, userId, postId)
	if err != nil {
		return err
	}

	return nil
}
