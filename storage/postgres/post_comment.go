package postgres

import (
	"blog-post/models"
	"context"
	"database/sql"
	"fmt"

	"github.com/jackc/pgx/v4/pgxpool"
)

type postCommentRepo struct {
	db *pgxpool.Pool
}

func NewPostCommentRepo(db *pgxpool.Pool) *postCommentRepo {
	return &postCommentRepo{
		db: db,
	}
}

func (r *postCommentRepo) Create(ctx context.Context, req *models.CommentCreateReq) (resp *int, err error) {
	query := `
	INSERT INTO posts_comments(
		post_id,
		user_id,
		description
	)
	VALUES($1,$2,$3)
	RETURNING id;`

	if err = r.db.QueryRow(ctx, query, req.PostId, req.UserId, req.Description).Scan(&resp); err != nil {
		return nil, err
	}

	return resp, nil
}

func (r *postCommentRepo) GetList(ctx context.Context, req *models.CommentGetListReq) (*models.CommentGetListResp, error) {
	var (
		filter  = " WHERE deleted_at IS NULL "
		offsetQ = " OFFSET 0;"
		limit   = " LIMIT 10 "
		offset  = (req.Page - 1) * req.Limit
		count   int
		resp    = &models.CommentGetListResp{}
	)

	s := `
	SELECT 
		id,
		post_id,
		user_id,
		description,
		created_at::TEXT,
		updated_at::TEXT
	FROM posts_comments `

	if req.PostId != "" {
		filter += ` AND post_id='` + req.PostId + `' `
	}
	if req.Limit > 0 {
		limit = fmt.Sprintf("LIMIT %d", req.Limit)
	}
	if offset > 0 {
		offsetQ = fmt.Sprintf("OFFSET %d", offset)
	}

	query := s + filter + limit + offsetQ

	countS := `SELECT COUNT(*) FROM posts_comments ` + filter

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	err = r.db.QueryRow(ctx, countS).Scan(&count)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			id          int
			postId      string
			userId      string
			description string
			createdAt   string
			updatedAt   sql.NullString
		)
		err := rows.Scan(
			&id,
			&postId,
			&userId,
			&description,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return nil, err
		}

		resp.Comments = append(resp.Comments, &models.Comment{
			Id:          id,
			PostId:      postId,
			UserId:      userId,
			Description: description,
			CreatedAt:   createdAt,
			UpdatedAt:   updatedAt.String,
		})
		resp.Count = count
	}

	return resp, nil
}

func (r *postCommentRepo) GetById(ctx context.Context, req *models.CommentIdReq) (*models.Comment, error) {
	var (
		id          int
		postId      string
		userId      string
		description string
		createdAt   string
		updatedAt   sql.NullString
	)

	query := `
	SELECT 
		id,
		post_id,
		user_id,
		description,
		created_at::TEXT,
		updated_at::TEXT
	FROM posts
	WHERE id = $1 AND deleted_at IS NULL;`

	if err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&postId,
		&userId,
		&description,
		&createdAt,
		&updatedAt,
	); err != nil {
		return nil, err
	}

	return &models.Comment{
		Id:          id,
		PostId:      postId,
		UserId:      userId,
		Description: description,
		CreatedAt:   createdAt,
		UpdatedAt:   updatedAt.String,
	}, nil
}

func (r *postCommentRepo) Update(ctx context.Context, req *models.CommentUpdateReq) (resp *string, err error) {
	query := `
    UPDATE posts_comments
    SET description = $2,
		updated_at = NOW()
    WHERE id = $1;`

	_, err = r.db.Exec(ctx, query,
		req.Id,
		req.Description,
	)

	if err != nil {
		return nil, err
	}

	*resp = "OK"
	return resp, nil
}
