package postgres

import (
	"blog-post/config"
	"blog-post/storage"
	"context"
	"fmt"

	"github.com/jackc/pgx/v4/pgxpool"
)

type store struct {
	db            *pgxpool.Pool
	posts         *postRepo
	users         *userRepo
	postsLikes    *postLikeRepo
	postsComments *postCommentRepo
	commentsLikes *commentLikeRepo
}

func NewStorage(ctx context.Context, cfg config.Config) (storage.StorageI, error) {
	config, err := pgxpool.ParseConfig(
		fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=disable",
			cfg.PostgresUser,
			cfg.PostgresPassword,
			cfg.PostgresHost,
			cfg.PostgresPort,
			cfg.PostgresDatabase,
		),
	)

	if err != nil {
		fmt.Println("ParseConfig:", err.Error())
		return nil, err
	}

	pool, err := pgxpool.ConnectConfig(ctx, config)
	if err != nil {
		fmt.Println("ConnectConfig:", err.Error())
		return nil, err
	}

	return &store{
		db: pool,
	}, nil
}

func (s *store) Post() storage.PostsI {
	if s.posts == nil {
		s.posts = NewPostRepo(s.db)
	}

	return s.posts
}

func (s *store) User() storage.UsersI {
	if s.users == nil {
		s.users = NewUserRepo(s.db)
	}

	return s.users
}

func (s *store) PostLike() storage.PostsLikesI {
	if s.postsLikes == nil {
		s.postsLikes = NewPostLikeRepo(s.db)
	}

	return s.postsLikes
}

func (s *store) PostComment() storage.PostsCommentsI {
	if s.postsComments == nil {
		s.postsComments = NewPostCommentRepo(s.db)
	}

	return s.postsComments
}

func (s *store) CommentLike() storage.CommentsLikesI {
	if s.commentsLikes == nil {
		s.commentsLikes = NewCommentLikeRepo(s.db)
	}

	return s.commentsLikes
}
