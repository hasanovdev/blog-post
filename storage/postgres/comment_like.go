package postgres

import (
	"context"
	"errors"

	"github.com/jackc/pgx/v4/pgxpool"
)

type commentLikeRepo struct {
	db *pgxpool.Pool
}

func NewCommentLikeRepo(db *pgxpool.Pool) *commentLikeRepo {
	return &commentLikeRepo{
		db: db,
	}
}

func (r *commentLikeRepo) Push(ctx context.Context, userId, commentId string) error {
	query := `
	INSERT INTO comments_likes(
		user_id,
		comment_id
	)
	VALUES($1,$2);`

	_, err := r.db.Exec(ctx, query, userId, commentId)
	if err != nil {
		return err
	}

	return nil
}

func (r *commentLikeRepo) Pop(ctx context.Context, userId, commentId string) error {
	query := `
	DELETE FROM comments_likes
	WHERE user_id = $1 AND comment_id = $2;`

	_, err := r.db.Exec(ctx, query, userId, commentId)
	if err != nil {
		return err
	}

	return nil
}

func (r *commentLikeRepo) Check(ctx context.Context, userId, commentId string) error {
	var count int
	query := `
	SELECT COUNT(*) FROM comments_likes
	WHERE user_id = $1 AND comment_id = $2;`

	if err := r.db.QueryRow(ctx, query, userId, commentId).Scan(&count); err != nil {
		return err
	}

	if count == 0 {
		return errors.New("count=0")
	}

	return nil
}

func (r *commentLikeRepo) Count(ctx context.Context, commentId string) (count int, err error) {
	query := `
	SELECT COUNT(*) FROM comments_likes
	WHERE comment_id = $1;`

	if err := r.db.QueryRow(ctx, query, commentId).Scan(&count); err != nil {
		return 0, err
	}

	return count, nil
}
