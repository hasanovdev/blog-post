package postgres

import (
	"blog-post/models"
	"context"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type userRepo struct {
	db *pgxpool.Pool
}

func NewUserRepo(db *pgxpool.Pool) *userRepo {
	return &userRepo{
		db: db,
	}
}

func (r *userRepo) Create(ctx context.Context, req *models.UserCreateReq) (*models.UserCreateResp, error) {
	id := uuid.NewString()

	query := `
	INSERT INTO users(
        id,
        username,
        password
	)
	VALUES ($1, $2, $3);
	`

	_, err := r.db.Exec(ctx, query,
		id,
		req.Username,
		req.Password,
	)

	if err != nil {
		return nil, err
	}

	return &models.UserCreateResp{
		Msg: "successfully created user",
	}, nil
}

func (r *userRepo) GetByUsername(ctx context.Context, req *models.ReqByUsername) (*models.User, error) {
	query := `
    SELECT 
		id,
        username,
        password,
        is_active
	FROM users WHERE username = $1 AND deleted_at IS NULL;
    `

	var user = models.User{}

	err := r.db.QueryRow(ctx, query, req.Username).Scan(
		&user.Id,
		&user.Username,
		&user.Password,
		&user.IsActive,
	)

	if err != nil {
		return nil, err
	}

	return &user, nil
}
