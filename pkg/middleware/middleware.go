package middleware

import (
	"blog-post/config"
	"blog-post/models"
	"blog-post/pkg/helper"
	"net/http"

	"github.com/gin-gonic/gin"
)

func AuthMiddleware(c *gin.Context) {
	tokenString := c.GetHeader("Authorization")

	if tokenString == "" {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Unauthorized"})
		c.Abort()
		return
	}

	token, _ := helper.ExtractToken(tokenString)
	userInfo, err := helper.ParseClaims(token, config.JWTSecretKey)
	if err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "error while parse token"})
		c.Abort()
		return
	}

	c.Set("user_info", userInfo)

	c.Next()
}

func ValidateMiddleware(c *gin.Context) {
	var req models.SignInReq
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		c.Abort()
		return
	}

	if helper.IsValidPassword(req.Password) {
		c.Next()
	} else {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "password is invalid"})
		c.Abort()
	}
}
