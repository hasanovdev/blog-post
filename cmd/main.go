package main

import (
	"blog-post/api"
	"blog-post/api/handler"
	"blog-post/config"
	"blog-post/pkg/logger"
	"blog-post/storage/postgres"
	"context"
)

func main() {
	cfg := config.Load()
	log := logger.NewLogger("mini-project", logger.LevelInfo)
	strg, err := postgres.NewStorage(context.Background(), *cfg)
	if err != nil {
		panic(err)
	}

	h := handler.NewHandler(strg, *cfg, log)

	r := api.NewServer(h)
	r.Run("localhost:8080")
}
